﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int _numberOfCellsHigh;
        private int _numberOfCellsWide;
        private int _minNeighbours;
        private int _maxNeighbours;
        private int _cellSideLength;
        private int _cellSideHeight;
        private double _cellHorizPadding;
        private double _cellVertPadding;
        private List<double> _cellHorizPositions;
        private List<double> _cellVertPositions;
        private static readonly Color _deadCellColour = Color.FromRgb(40, 40, 40);
        private static readonly Color _liveCellColour = Color.FromRgb(0, 200, 0);
        private static int _strokeThickness;
        private bool[,] _gameCells;
        private bool[,] _manualGameCells;
        private DispatcherTimer _generationTimer;
        private bool _isRunning = false;
        private int _generation = 0;
        private ManualResetEventSlim _waitForGameToStop = new ManualResetEventSlim();

        public MainWindow()
        {
            InitializeComponent();
            SetCellSizeAndLayout();
            _minNeighbours = Int32.Parse(minimumNeighbours.Text);
            _maxNeighbours = Int32.Parse(maximumNeighbours.Text);
            ResetGame();
        }

        private void SetCellSizeAndLayout()
        {
            GetCellCount();
            _gameCells = new bool[_numberOfCellsHigh + 2, _numberOfCellsWide + 2];
            _manualGameCells = new bool[_numberOfCellsHigh + 2, _numberOfCellsWide + 2];
            SetCellSize();
            SetCellLayout();
        }

        private void GetCellCount()
        {
            _numberOfCellsWide = (int)widthValue.Value;
            _numberOfCellsHigh = (int)heightValue.Value;
        }

        private void SetCellSize()
        {
            _cellSideLength = ((int)lifeBoard.Width - (_numberOfCellsWide - 2)) / _numberOfCellsWide;
            _cellSideHeight = ((int)lifeBoard.Height - (_numberOfCellsHigh - 2)) / _numberOfCellsHigh;
            _strokeThickness = Math.Max(_cellSideLength, _cellSideHeight);
            _cellHorizPadding = (lifeBoard.Width - _numberOfCellsWide * _cellSideLength)
                                / (_numberOfCellsWide - 2);
            _cellVertPadding = (lifeBoard.Height - _numberOfCellsHigh * _cellSideHeight)
                                / (_numberOfCellsHigh - 2);
        }

        private void SetCellLayout()
        {
            _cellHorizPositions = new List<double>(Enumerable.Repeat(0d, _numberOfCellsWide));
            _cellVertPositions = new List<double>(Enumerable.Repeat(0d, _numberOfCellsHigh));

            Enumerable.Range(0, _numberOfCellsWide).ForEach(
                col => _cellHorizPositions[col] = (_cellSideLength + _cellHorizPadding) * col + 1);
            Enumerable.Range(0, _numberOfCellsHigh).ForEach(
                    row => _cellVertPositions[row] = (_cellSideHeight + _cellVertPadding) * row + 1);
        }

        private void ResetGame()
        {
            _waitForGameToStop.Reset();
            ResetLife();
            FillWithDeadCells();

            if (!isManualPatternEntryEnabled.IsChecked.Value)
            {
                GenerateRandomStartingCells();
            }
            else
            {
                _gameCells = _manualGameCells;
            }

            DrawCurrentGeneration();
        }

        private void ResetLife()
        {
            _generation = 0;
            generation.Text = _generation.ToString();
            _gameCells = new bool[_numberOfCellsHigh + 2, _numberOfCellsWide + 2];
        }

        private void FillWithDeadCells()
        {
            lifeBoard.Children.Clear();

            Enumerable.Range(0, _numberOfCellsHigh).ForEach(
                row =>
                    Enumerable.Range(0, _numberOfCellsWide)
                        .ForEach(col => DrawDeadCell(_cellHorizPositions[col], _cellVertPositions[row])));
        }

        private void DrawDeadCell(double x, double y)
        {
            DrawCell(x, y, _deadCellColour);
        }

        private void DrawCell(double x, double y, Color cellColour)
        {
            Rectangle cell = new Rectangle();

            cell.Width = _cellSideLength;
            cell.Height = _cellSideHeight;

            SolidColorBrush cellBrush = new SolidColorBrush(cellColour);

            cell.StrokeThickness = _strokeThickness;
            cell.Fill = cellBrush;
            Canvas.SetLeft(cell, x);
            Canvas.SetTop(cell, y);
            lifeBoard.Children.Add(cell);
        }

        private void DrawLivingCell(double x, double y)
        {
            DrawCell(x, y, _liveCellColour);
        }

        private void GenerateRandomStartingCells()
        {
            Random generator = new Random();
            int numberOfStartCells =
                (int)((decimal)(percCoverValue.Value * _numberOfCellsHigh * _numberOfCellsWide) / 100M);
            int cellsAdded = 0;

            while (cellsAdded < numberOfStartCells)
            {
                int row = generator.Next(_numberOfCellsHigh);
                int col = generator.Next(_numberOfCellsWide);

                if (_gameCells[row + 1, col + 1])
                {
                    continue;
                }

                _gameCells[row + 1, col + 1] = true;
                ++cellsAdded;
            }
        }

        private void DrawCurrentGeneration()
        {
            lifeBoard.Children.Clear();
            Enumerable.Range(0, _numberOfCellsHigh).ForEach(
                row =>
                    Enumerable.Range(0, _numberOfCellsWide)
                        .ForEach(
                            col =>
                            {
                                if (_gameCells[row + 1, col + 1])
                                {
                                    DrawLivingCell(_cellHorizPositions[col], _cellVertPositions[row]);
                                }
                                else
                                {
                                    DrawDeadCell(_cellHorizPositions[col], _cellVertPositions[row]);
                                }
                            }));
        }

        private void runStopBtn_Click(object sender, RoutedEventArgs e)
        {
            runStopBtn.IsEnabled = false;

            if (!_isRunning)
            {
                percCoverValue.IsEnabled = false;
                widthValue.IsEnabled = false;
                heightValue.IsEnabled = false;
                resetBtn.IsEnabled = false;
                isManualPatternEntryEnabled.IsEnabled = false;
                delayValue.IsEnabled = false;
                runStopBtn.Content = "Stop";
                _generationTimer = new DispatcherTimer(
                    TimeSpan.FromMilliseconds(Int32.Parse(delayValue.Text)),
                    DispatcherPriority.ApplicationIdle,
                    OnGenerate,
                    Dispatcher.CurrentDispatcher);

                _waitForGameToStop.Reset();
                _generationTimer.Start();
                _isRunning = true;
            }
            else
            {
                _generationTimer.Stop();
                _generationTimer = null;
                _isRunning = false;
                runStopBtn.Content = "Run";
                resetBtn.IsEnabled = true;
                percCoverValue.IsEnabled = true;
                widthValue.IsEnabled = true;
                heightValue.IsEnabled = true;
                isManualPatternEntryEnabled.IsEnabled = true;
                delayValue.IsEnabled = true;
                _waitForGameToStop.Set();
            }

            runStopBtn.IsEnabled = true;
        }

        private void OnGenerate(object sender, EventArgs e)
        {
            _generationTimer.Stop();

            int[,] livingNeighboursCounts = CalculateLivingNeighbours();
            int remainingLivingCells = GenerateNextGeneration(livingNeighboursCounts);

            generation.Text = _generation.ToString();
            DrawCurrentGeneration();

            if (remainingLivingCells == 0)
            {
                runStopBtn_Click(runStopBtn, new RoutedEventArgs());
                return;
            }

            _generationTimer.Start();
        }

        private int[,] CalculateLivingNeighbours()
        {
            int[,] livingNeighboursByRow = new int[_numberOfCellsHigh + 2, _numberOfCellsWide];

            // In row y, column x, 
            // where 0 <= y <= <living cells row count> and 1 <= x <= <living cells column count - 1>, 
            // count the number of living cells in columns x - 1, x and x + 1.
            Enumerable.Range(0, _numberOfCellsHigh + 2).ForEach(
                row =>
                {
                    Enumerable.Range(1, _numberOfCellsWide - 1).ForEach(
                        col =>
                        {
                            livingNeighboursByRow[row, col] = LivingCellToInt(_gameCells[row, col - 1])
                                                              + LivingCellToInt(_gameCells[row, col])
                                                              + LivingCellToInt(_gameCells[row, col + 1]);
                        });
                });

            return livingNeighboursByRow;
        }

        private int LivingCellToInt(bool isLivingCell)
        {
            return isLivingCell ? 1 : 0;
        }

        //private int GenerateNextGeneration(Pair<int[,], int[,]> livingNeighboursCounts)
        private int GenerateNextGeneration(int[,] livingNeighboursCounts)
        {
            bool[,] nextGeneration = new bool[_numberOfCellsHigh + 2, _numberOfCellsWide + 2];
            int livingCellCount = 0;

            Enumerable.Range(1, _numberOfCellsHigh).ForEach(
                row =>
                {
                    Enumerable.Range(1, _numberOfCellsWide - 1).ForEach(
                        col =>
                        {
                            // Apply the rules of the game:
                            // A live cell with fewer then 2 or more than 3 live neighbours dies.
                            // A "dead" cell with exactly 3 live neighbours comes to life.
                            int livingNeighbours = CountLivingNeighbours(row, col, livingNeighboursCounts);

                            if (_gameCells[row, col])
                            {
                                if (livingNeighbours < 2 || livingNeighbours > 3)
                                {
                                    nextGeneration[row, col] = false;
                                }
                                else
                                {
                                    nextGeneration[row, col] = true;
                                    ++livingCellCount;
                                }
                            }
                            else
                            {
                                if (livingNeighbours == 3)
                                {
                                    nextGeneration[row, col] = true;
                                    ++livingCellCount;
                                }
                                else
                                {
                                    nextGeneration[row, col] = false;
                                }
                            }
                        });
                });

            _gameCells = nextGeneration;
            ++_generation;
            return livingCellCount;
        }

        //private int CountLivingNeighbours(int row, int col, Pair<int[,], int[,]> livingNeighboursCounts)
        private int CountLivingNeighbours(int row, int col, int[,] livingNeighboursCounts)
        {
            // The count of living neighbours takes into account the count in the rows above and below and the cells to
            // the left and right.  For the first cell in the "active" grid, the first column of the first row of counts
            // gives the number of living neighbours above and the first column of the third row of counts for the row
            // below.  The neighbours left and right are simply checked in the collection of cells.
            //int livingCellCountInRowAbove = livingNeighboursCounts.First[row - 1, col];
            //int livingCellCountInRowBelow = livingNeighboursCounts.First[row + 1, col];
            int livingCellCountInRowAbove = livingNeighboursCounts[row - 1, col];
            int livingCellCountInRowBelow = livingNeighboursCounts[row + 1, col];
            int livingCellCountOnEitherSide = LivingCellToInt(_gameCells[row, col - 1])
                                              + LivingCellToInt(_gameCells[row, col + 1]);

            return livingCellCountInRowAbove + livingCellCountOnEitherSide + livingCellCountInRowBelow;
        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            ResetGame();
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (_isRunning)
            {
                runStopBtn_Click(runStopBtn, new RoutedEventArgs());
                _waitForGameToStop.Wait();
            }

            Close();
        }

        private void percCoverValue_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResetGame();
        }

        private void percCoverValue_TouchLeave(object sender, System.Windows.Input.TouchEventArgs e)
        {
            ResetGame();
        }

        private void widthValue_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SetCellSizeAndLayout();
            ResetGame();
        }

        private void heightValue_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SetCellSizeAndLayout();
            ResetGame();
        }

        private void isManualPatternEntryEnabled_Checked(object sender, RoutedEventArgs e)
        {
            clearManualPattern.IsEnabled = true;
            ResetLife();
            FillWithDeadCells();
        }

        private void isManualPatternEntryEnabled_Unchecked(object sender, RoutedEventArgs e)
        {
            clearManualPattern.IsEnabled = false;
            ResetGame();
        }

        private void lifeBoard_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!isManualPatternEntryEnabled.IsChecked.Value || _isRunning)
            {
                return;
            }

            Rectangle selectedCell = e.OriginalSource as Rectangle;

            if (null == selectedCell)
            {
                return;
            }

            Point mousePos = e.GetPosition(lifeBoard);

            int horizCellindex = _cellHorizPositions.IndexOf(_cellHorizPositions.Last(pos => pos < mousePos.X));
            int vertCellindex = _cellVertPositions.IndexOf(_cellVertPositions.Last(pos => pos < mousePos.Y));
            bool isCellAlive = _manualGameCells[vertCellindex + 1, horizCellindex + 1];
            Color newCellColour = isCellAlive ? _deadCellColour : _liveCellColour;

            lifeBoard.Children.Remove(selectedCell);
            DrawCell(_cellHorizPositions[horizCellindex], _cellVertPositions[vertCellindex], newCellColour);
            _gameCells[vertCellindex + 1, horizCellindex + 1] = !isCellAlive;
            _manualGameCells[vertCellindex + 1, horizCellindex + 1] = !isCellAlive;
        }

        private void minimumNeighbours_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex minNeighboursPattern = new Regex("^[1-4]$");
            bool isValidNumber = minNeighboursPattern.IsMatch(e.Text);
            int newValue = isValidNumber ? Int32.Parse(e.Text) : _minNeighbours;

            e.Handled = !isValidNumber || newValue >= _maxNeighbours;
        }

        private void clearManualPattern_Click(object sender, RoutedEventArgs e)
        {
            if (isManualPatternEntryEnabled.IsChecked.Value && !_isRunning)
            {
                _manualGameCells = new bool[_numberOfCellsHigh + 2, _numberOfCellsWide + 2];
                FillWithDeadCells();
            }
        }

        private Regex _delayPattern = new Regex("^[0-9]$");

        private void delayValue_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !_delayPattern.IsMatch(e.Text);
        }

        private void delayValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (delayValue.Text.Length > 3)
            {
                int caretIndex = delayValue.CaretIndex;
                string firstPart = delayValue.Text.Substring(0, caretIndex - 1);
                string lastPart = delayValue.Text.Substring(caretIndex);
                delayValue.Text = firstPart + lastPart;
                delayValue.CaretIndex = caretIndex - 1;
                e.Handled = true;
            }
        }
    }
}
